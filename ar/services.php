<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body>
		<div id="menu" class="hide sticky">
			<div class="container">
				<div class="col m3">
					<a href="javascript:;" class="logo left">
						<img src="../dist/img/inn-go-hotel-black.png" srcset="../dist/img/inn-go-hotel-black.png 1x, ../dist/img/inn-go-hotel-black@2x.png 2x, ../dist/img/inn-go-hotel-black@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="144" height="40">
					</a>
				</div>
				<div class="col m1 right">
					<button class="hamburger hamburger--squeeze right open-modal" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="check-booking col m8 left">
					<div class="left calendar">
						<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<div class="left calendar">
						<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
				</div>
			</div>
		</div>
		<div id="hero" class="intern">
			<div class="content">
				<div class="clearfix">
					<a href="javascript:;" class="left">
						<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class="logo ">
					</a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>Ar</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">خدمات</h2>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern" class="clearfix right-align">
			<div class="info">
				<h1 class="f192 hd-medium center-align">خدمات</h1>
				<div class="col m3 center-block">
					<p class="a-light f18 center-align">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes, 24 hours in- room dining, laundry service, safety deposit box.</p>
				</div>
			</div>
			<div class="bg-graylight clearfix">
				<div class="container clearfix">
					<div class="row-cards">
						<div class="card col m6">
							<div class="card-image">
								<img class="responsive-img" src="http://placehold.it/1881x1551">
							</div>
							<div class="card-content">
								<div class="name">
									<h2 class="activator center-align f58 hd-light">Standard</h2>
									<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض المزيد</a>
								</div>
								<div class="extended hide no-booking">
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<h2 class="activator center-align f58 hd-light">Standard</h2>
										<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض أقل</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card col m6">
							<div class="card-image">
								<img class="responsive-img" src="http://placehold.it/1881x1551">
							</div>
							<div class="card-content">
								<div class="name">
									<h2 class="activator center-align f58 hd-light">Deluxe</h2>
									<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض المزيد</a>
								</div>
								<div class="extended hide no-booking">
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<h2 class="activator center-align f58 hd-light">Deluxe</h2>
										<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض أقل</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row-cards">
						<div class="card col m6">
							<div class="card-image">
								<img class="responsive-img" src="http://placehold.it/1881x1551">
							</div>
							<div class="card-content">
								<div class="name">
									<h2 class="activator center-align f58 hd-light">Business Suite</h2>
									<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض المزيد</a>
								</div>
								<div class="extended hide no-booking">
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<h2 class="activator center-align f58 hd-light">Business Suite</h2>
										<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض أقل</a>
									</div>
								</div>
							</div>
						</div>
						<div class="card col m6">
							<div class="card-image">
								<img class="responsive-img" src="http://placehold.it/1881x1551">
							</div>
							<div class="card-content">
								<div class="name">
									<h2 class="activator center-align f58 hd-light">Executive Room</h2>
									<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض المزيد</a>
								</div>
								<div class="extended hide no-booking">
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<div class="col m11">
											<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes.</p>
										</div>
									</div>
									<div class="col m4">
										<h2 class="activator center-align f58 hd-light">Executive Room</h2>
										<a href="javascript:;" class="view-more btn-large f20 a-regular white-text">عرض أقل</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-right col m3">
					<a href="./" class="hd-medium f44 white-text right-align"><span>الصفحة الرئيسية</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>معلومات عنا</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أماكن الإقامة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>المطاعم</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>مرافق</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align active">خدمات</a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>اتصل بنا</span></a>
				</div>
				<div class="text-left col m3">
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خبرة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أحداث</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>رجال الإعلام</span></a>
					<div class="newsletter right">
						<p class="white-text f18 a-light right-align">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light right-align" required>
									<label for="email" data-error="خاطئ" class="right-align"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>app.viewMore();</script>
	</body>
</html>