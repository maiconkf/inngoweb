<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body>
		<div id="menu" class="hide sticky">
			<div class="container">
				<div class="col m3">
					<a href="javascript:;" class="logo left">
						<img src="../dist/img/inn-go-hotel-black.png" srcset="../dist/img/inn-go-hotel-black.png 1x, ../dist/img/inn-go-hotel-black@2x.png 2x, ../dist/img/inn-go-hotel-black@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="144" height="40">
					</a>
				</div>
				<div class="col m1 right">
					<button class="hamburger hamburger--squeeze right open-modal" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="check-booking col m8 left">
					<div class="left calendar">
						<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<div class="left calendar">
						<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
				</div>
			</div>
		</div>
		<div id="hero" class="home">
			<div class="content">
				<div class="clearfix">
					<a href="javascript:;" class="left">
						<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class="logo ">
					</a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>Ar</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">The best way to experience what Kuwait has to offer.</h2>
					</div>
				</div>
				<div class="row">
					<div class="center-block box-scroll">
						<p class="f20 white-text center-align a-lightitalic">Scroll to discover</p>
						<a href="javascript:;" class="arrow-scroll bounce icon"></a>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">التحقق من توافر</a>
					</div>
				</div>
			</div>
		</div>
		<div id="rooms">
			<div class="container clearfix">
				<ul>
					<li id="cards-1" class="col m5 clearfix scrollfire-left">
						<h3 class="hd-medium f58 right-align">أماكن الإقامة</h3>
						<p class="a-light f16 right-align">
							Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes, 24 hours in- room dining, laundry service, safety deposit box.
						</p>
						<a href="javascript:;" class="btn-large f20 a-light right">انظر جميع الغرف</a>
					</li>
				</ul>
				<ul id="cards-2">
					<li class="col m7 right scrollfire-right">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block right-align">
							<h3 class="hd-medium f60">Business Suite</h3>
							<p class="a-light f16">High speed internet access, Satellite TV including international channels, in-room safe, individually controlled air conditioning system, IDD telephone line, coffee & tea facilities, mini bar, daily news papers and hair dryer. In-room personal computer included in certain rooms.</p>
						</div>
					</li>
				</ul>
				<ul id="cards-3">
					<li class="col m5 left scrollfire-left">
						<img class="responsive-img" src="http://placehold.it/599x737">
						<div class="col m11 center-block right-align">
							<h3 class="hd-medium f60">Executive</h3>
							<p class="a-light f16">Each of our Executive rooms is thoughtfully designed for both the business and leisure traveler. Large bathrooms with shower and Jacuzzi are also available.</p>
						</div>
					</li>
				</ul>
				<ul id="cards-4">
					<li class="col m7 right scrollfire-right">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block right-align">
							<h3 class="hd-medium f60">Deluxe</h3>
							<p class="a-light f16">Deluxe rooms come with a range of upgraded value-added benefits such as Personal Computer in the room to suit business or leisure travelers. In addition, rooms on the 10th floor are equipped with extra toiletries, bathtub, and separate shower.</p>
						</div>
					</li>
				</ul>
				<ul id="cards-5">
					<li class="col m5 left scrollfire-left">
						<img class="responsive-img" src="http://placehold.it/599x737">
						<div class="col m11 center-block right-align">
							<h3 class="hd-medium f60">Standard</h3>
							<p class="a-light f16">Each of our Executive rooms is thoughtfully designed for both the business and leisure traveler. Large bathrooms with shower and Jacuzzi are also available.</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div id="restaurants">
			<div class="container">
				<h2 class="f200 hd-medium col offset-m1 right-align">المطاعم</h2>
				<div class="offset-m1 col m6 right-align">
					<p class="f22 a-light">Taste some of the most delicious international cuisine at Al-Dallah restaurant offers cuisine from around the world. The restaurant features a daily buffet breakfast and extensive á la carte lunch and dinner menus with international flavors.</p>
				</div>
			</div>
			<div class="types">
				<div class="clearfix">
					<div class="col m6 right">
						<img class="responsive-img right" src="http://placehold.it/959x930">
					</div>
					<div class="col left m5 offset-m1">
						<div class="col m12 right-align">
							<h3 class="f60 hd-medium col m8">Arrigang</h3>
							<p class="f18 a-light col m8">Arirang Restaurant offers traditional Korean cuisine with an exceptional ambiance and unique furnishing.</p>
							<div class="col m8">
								<a href="javascript:;" class="btn-large f20 a-light right">المزيد من التفاصيل؟</a>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix">
					<div class="col m6 left mob">
						<img class="responsive-img left" src="http://placehold.it/959x930">
					</div>
					<div class="col right m5 offset-m1">
						<div class="col m12 right-align">
							<h3 class="f60 hd-medium col m8 right-align">Arrigang</h3>
							<p class="f18 a-light col m8">Arirang Restaurant offers traditional Korean cuisine with an exceptional ambiance and unique furnishing.</p>
							<div class="col m8">
								<a href="javascript:;" class="btn-large f20 a-light right">المزيد من التفاصيل؟</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="more" class="clearfix">
			<div class="container right-align">
				<h2 class="f120 hd-medium white-text center-align">أكثر؟</h2>
				<div class="clearfix">
					<div class="col m12 center-block">
						<div class="col m5 left offset-m1">
							<div class="col m11 box-news white right">
								<h3 class="hd-medium f103">أخبار</h3>
								<p class="a-light f20">Be updated on everything that’s happening on your network. Fabio is awesome too and this copy isn’t final.</p>
								<a href="javascript:;" class="a-regular f20 right">أعرف أكثر</a>
							</div>
						</div>
						<div class="col m5 right">
							<div class="col m11 box-experiences white">
								<img class="responsive-img left" src="http://placehold.it/460x549">
								<div class="description left">
									<h3 class="hd-medium f50">خبرة</h3>
									<p class="a-light f20">Be updated on everything that’s happening on your network. Fabio is awesome too and this copy isn’t final.</p>
									<a href="javascript:;" class="a-regular f20 right">أعرف أكثر</a>
								</div>
							</div>
						</div>
						<div class="col m6 left">
							<div class="box-facilities white clearfix">
								<img class="responsive-img left" src="http://placehold.it/665x373">
								<div class="description left">
									<h3 class="hd-medium f50">مرافق</h3>
									<p class="a-light f20">Our facilities are state-of-the-art and the best you can find in Kuwait. We rock, bitch. Fabio is awesome too and this copy isn’t final.</p>
									<a href="javascript:;" class="a-regular f20 right">أعرف أكثر</a>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-right col m3">
					<a href="./" class="hd-medium f44 white-text right-align">الصفحة الرئيسية</a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>معلومات عنا</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أماكن الإقامة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>المطاعم</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>مرافق</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خدمات</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>اتصل بنا</span></a>
				</div>
				<div class="text-left col m3">
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خبرة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أحداث</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>رجال الإعلام</span></a>
					<div class="newsletter right">
						<p class="white-text f18 a-light right-align">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light right-align" required>
									<label for="email" data-error="خاطئ" class="right-align"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>
			function isScrolledIntoView(elem) {
			    var docViewTop = $(window).scrollTop();
			    var docViewBottom = docViewTop + $(window).height();
			    var elemTop = $(elem).offset().top;
			    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
			}

			$(window).scroll(function () {
			    $('.scrollfire-left').each(function (i) {
			        if (isScrolledIntoView(this)) {
			            $(this).addClass("slideRight");
			        }
			    });

			    $('.scrollfire-right').each(function (i) {
			        if (isScrolledIntoView(this)) {
			            $(this).addClass("slideLeft");
			        }
			    });
			});
		</script>
	</body>
</html>