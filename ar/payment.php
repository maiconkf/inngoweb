<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two" class="no-booking">
		<?php include_once('includes/header.php'); ?>
		<div id="booking" class="clearfix black">
			<div class="container payment">
				<div class="col m7 center-block">
					<ul class="clearfix">
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text right-align">1. حدد غرفة</a></li>
						<li class="f24 left active"><a href="javascript:;" class="hd-light white-text right-align">2. دفع</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text right-align">3. مراجعة</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text right-align">4. التأكيد</a></li>
					</ul>
				</div>
				<p class="hd-medium f124 white-text center-align"><i></i>دفع</p>
			</div>
		</div>
		<div class="container clearfix">
			<div class="col m7 w1200 right right-align">
				<div class="col m10 right valign-wrapper valign-review">
					<p class="f180 hd-light right-align valign">مراجعة</p>
				</div>
			</div>
			<div class="box-review col m5 big-block">
				<div class="col m11 clearfix center-block">
					<div class="col m10 center-block clearfix">
						<a href="javascript:;" class="logo">
							<img src="../dist/img/inn-go-hotel-bigger.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class=" center-block">
						</a>
						<div class="block">
							<p class="f20 hd-medium">9 nights</p>
							<p class="f20 a-light">From 12-04-2016 to 22-04-2016</p>
						</div>
						<div class="block">
							<p class="f20 hd-medium">Business Executive Room</p>
							<p class="f20 a-light">2 Adults</p>
						</div>
						<div class="block">
							<p class="f32 hd-medium">2,480.00 EUR</p>
							<p class="f20 a-light">/Total Stay</p>
						</div>
						<a href="javascript:;" class="btn center-block a-regular f18"><span class="left">تفاصيل</span> <i class="material-icons">trending_flat</i></a>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5">
					<div class="col m10 left valign-wrapper valign-guest">
						<p class="f180 hd-light left-align valign right-align">ضيف</p>
					</div>
				</div>
				<div class="box-guest col m7">
					<div class="col m12 right">
						<div class="col m10 center-block clearfix">
							<div class="input-field clearfix">
								<input placeholder="الاسم الاول" id="first-name" name="first-name" type="text" class="validate a-light f22 right-align" required>
								<label for="first-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="الكنية" id="last-name" name="last-name" type="text" class="validate a-light f22 right-align" required>
								<label for="last-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="بريدك الالكتروني" id="email" name="email" type="email" class="validate a-light f22 right-align" required>
								<label for="email" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="هاتف" id="tel" name="tel" type="tel" class="validate a-light f22 right-align" required>
								<label for="tel" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="الرمز البريدي" id="zip" name="zip" type="text" class="validate a-light f22 right-align" required>
								<label for="zip" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="عنوان" id="address" name="address" type="text" class="validate a-light f22 right-align" required>
								<label for="address" data-error="خاطئ" class="right-align"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5 right">
					<div class="col m10 right valign-wrapper valign-card">
						<p class="f180 hd-light left-align valign right-align">بطاقة</p>
					</div>
				</div>
				<div class="box-card col m7">
					<div class="col m12 left">
						<div class="col m11 center-block clearfix">
							<div class="col m11">
								<p class="a-light f19"><i>نوع بطاقة الائتمان</i></p>
							</div>
							<a href="javascript:;" class="card-type a-regular f22 left active" data-type="visa">VISA</a>
							<a href="javascript:;" class="card-type a-regular f22 left" data-type="mastercard">Master Card</a>
							<a href="javascript:;" class="card-type a-regular f22 left" data-type="amex">American Express</a>
							<div class="input-field clearfix">
								<input placeholder="اسم حامل البطاقة" id="name-card" name="name-card" type="text" class="validate a-light f22 right-align" required>
								<label for="first-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="رقم بطاقة الائتمان" id="card-number" name="card-number" type="text" class="validate a-light f22 right-align" required>
								<label for="last-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="col m11 input-field">
								<p class="a-light f19"><i>تاريخ الانتهاء</i></p>
							</div>
							<div class="input-field col m4 date">
								<select>
									<option value="" disabled selected>شهر</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
		 					</div>
							<div class="input-field col m4 date">
								<select>
									<option value="" disabled selected>عام</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
								</select>
		 					</div>
							<div class="input-field col m4 cvc">
								<input placeholder="CVV/CVC" id="cvc" name="cvc" type="number" class="validate a-light f22 right-align" required>
								<label for="last-name" data-error="خاطئ" class="right-align"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5">
					<div class="col m10 left valign-wrapper valign-final">
						<p class="f180 hd-light left-align valign right-align">نهائي</p>
					</div>
				</div>
				<div class="box-final col m7">
					<div class="col m12 left">
						<div class="col m10 center-block clearfix">
							<div class="input-field clearfix">
								<input placeholder="أي معلومات إضافية؟ (اختياري)" id="name-card" name="name-card" type="text" class="validate a-light f22 right-align">
								<label for="first-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="رقم الرحلة (اختياري)" id="card-number" name="card-number" type="text" class="validate a-light f22 right-align">
								<label for="last-name" data-error="خاطئ" class="right-align"></label>
							</div>
							<div class="col m12 center-block clearfix box-agree right-align">
								<p class="f18 a-light col m10 left">لقد فهمت وأوافق على <a href="javascript:;" target="_blank" class="a-regular"><b>شروط الحجز</b></a> and <a href="javascript:;" target="_blank" class="a-regular"><b>سياسة الخصوصية</b></a></p>
								<div class="right col m2">
								    <input type="checkbox" id="agree" required />
								    <div class="checkbox">
								    	<span class="checked icon"></span>
								    </div>
								</div>
							</div>
							<button type="submit" class="btn center-block"><span class="left">الاستمرار في مراجعة تفاصيل</span> <i class="material-icons">trending_flat</i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-right col m3">
					<a href="./" class="hd-medium f44 white-text right-align"><span>الصفحة الرئيسية</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>معلومات عنا</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أماكن الإقامة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>المطاعم</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>مرافق</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خدمات</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>اتصل بنا</span></a>
				</div>
				<div class="text-left col m3">
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خبرة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أحداث</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>رجال الإعلام</span></a>
					<div class="newsletter right">
						<p class="white-text f18 a-light right-align">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light right-align" required>
									<label for="email" data-error="خاطئ" class="right-align"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>
			app.payment();
			app.init();
			$('select').material_select();
		</script>
	</body>
</html>