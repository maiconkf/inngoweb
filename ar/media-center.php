<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<?php include_once('includes/header.php'); ?>
		<div id="media" class="clearfix bg-gray">
			<div class="container">
				<h1 class="hd-light f136 center-align">المركز الإعلامي</h1>
			</div>
		</div>
		<div class="clearfix medias container">
			<div id="page5">
				<div class="full-h container">
					<div class="clearfix medias">
						<div class="col m7">
							<div class="card">
								<div class="card-image" style="background: url(//placehold.it/732x336);">
									<span class="f44 av-roman card-title">The story behind designing our website</span>
								</div>
								<div class="card-content">
									<div class="card-date">
										<em class="f17 a-lightitalic right-align right">March 05, 2016</em>
									</div>
									<div class="f17 av-light card-text right-align">
										<p>Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes. Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes...</p>
									</div>
								</div>
								<div class="card-action clearfix">
									<a href="javascript:;" class="f20 a-regular btn-large btn-large--landing left"><span class="left">مواصلة القراءة </span> <i class="material-icons">trending_flat</i></a>
									<a href="javascript:;" data-href="http://globo.com" class="right f17 a-lightitalic share-media">شارك <span class="icon icon-share right"></span></a>
								</div>
							</div>
							<div class="card">
								<div class="card-image" style="background: url(//placehold.it/732x336);">
									<span class="f44 av-roman card-title">The story behind designing our website</span>
								</div>
								<div class="card-content">
									<div class="card-date">
										<em class="f17 a-lightitalic right-align right">March 05, 2016</em>
									</div>
									<div class="f17 av-light card-text right-align">
										<p>Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes. Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes...</p>
									</div>
								</div>
								<div class="card-action clearfix">
									<a href="javascript:;" class="f20 a-regular btn-large btn-large--landing left"><span class="left">مواصلة القراءة </span> <i class="material-icons">trending_flat</i></a>
									<a href="javascript:;" data-href="http://globo.com" class="right f17 a-lightitalic share-media">شارك <span class="icon icon-share right"></span></a>
								</div>
							</div>
						</div>
						<div class="col m5">
							<div class="quote">
								<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over <a href="https://twitter.com/GrandTetonNPS">@GrandTetonNPS</a>. <a href="https://twitter.com/hashtag/nature?src=hash">#nature</a> <a href="https://twitter.com/hashtag/sunset?src=hash">#sunset</a> <a href="http://t.co/YuKy2rcjyU">pic.twitter.com/YuKy2rcjyU</a></p>&mdash; US Dept of Interior (@Interior) <a href="https://twitter.com/Interior/status/463440424141459456">5 de maio de 2014</a></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>

							<div class="quote">
								<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/tsxp1hhQTG/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">We’re putting the Weekend Hashtag Project on hold this weekend. Instead, we’re challenging people around the world to participate in the 10th Worldwide InstaMeet! Grab a few good friends or meet up with a larger group in your area and شارك your best photos and videos from the InstaMeet with the #WWIM10 hashtag for a chance to be featured on our blog Monday morning. Be sure to include the name of the location where your event took place along with the unique hashtag you&#39;ve chosen for your InstaMeet in your caption. Photo by @sun_shinealight</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Uma foto publicada por Instagram (@instagram) em <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2014-10-03T18:00:13+00:00">Out 3, 2014 às 11:00 PDT</time></p></div></blockquote><script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
							</div>
							<div class="quote">
								<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/" data-width="509"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/20531316728/posts/10154009990506729/">Publicado por <a href="https://www.facebook.com/facebook/">Facebook</a> em&nbsp;<a href="https://www.facebook.com/20531316728/posts/10154009990506729/">Quinta, 27 de agosto de 2015</a></blockquote></div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-right col m3">
					<a href="./" class="hd-medium f44 white-text right-align"><span>الصفحة الرئيسية</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>معلومات عنا</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أماكن الإقامة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>المطاعم</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>مرافق</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خدمات</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>اتصل بنا</span></a>
				</div>
				<div class="text-left col m3">
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خبرة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أحداث</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align active">رجال الإعلام</a>
					<div class="newsletter right">
						<p class="white-text f18 a-light right-align">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light right-align" required>
									<label for="email" data-error="خاطئ" class="right-align"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>
			$(".fb-post").attr("data-width", $(".fb-post").parent().width());
			$(window).on('resize', function () { resizeiframe(); });
			function resizeiframe() {
				var src = $('.fb-post iframe').attr('src').split('width='),
				    width = $(".fb-post").parent().width();

				$('.fb-post iframe').attr('src', src[0] + 'width=' + width);
			}
		</script>
	</body>
</html>