<!DOCTYPE html>
<html lang="ar">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two" class="no-booking">
		<?php include_once('includes/header.php'); ?>
		<div id="booking" class="clearfix black">
			<div class="container select-room">
				<div class="col m7 center-block">
					<ul class="clearfix">
						<li class="f24 left active"><a href="javascript:;" class="hd-light white-text">1. حدد غرفة</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">2. دفع</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">3. مراجعة</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">4. التأكيد</a></li>
					</ul>
				</div>
				<p class="hd-medium f124 white-text center-align"><i></i>حدد غرفة</p>
			</div>
		</div>
		<div class="container clearfix select-room">
			<div class="filter col m3 left">
				<div class="col m9 center-block clearfix">
					<form method="post" id="filter">
						<p class="f18 a-light right-align"><i>تحقق في</i></p>
						<div class="left calendar">
							<input type="text" name="checkin" class="checkin datepicker f20 a-light" value="12/03">
							<div class="box-calendar"><span class="icon-calendar icon"></span></div>
						</div>
						<p class="f18 a-light right-align"><i>الدفع</i></p>
						<div class="left calendar">
							<input type="text" name="checkout" class="checkin datepicker f20 a-light" value="16/03">
							<div class="box-calendar"><span class="icon-calendar icon"></span></div>
						</div>
						<p class="f18 a-light right-align"><i>ضيوف</i></p>
						<div class="left calendar">
							<input type="text" name="checkout" class="f20 a-light" value="2 Guest" required>
							<div class="box-calendar"><span class="icon-guest icon"></span></div>
						</div>
						<input type="submit" value="تعديل" class="f20 a-light btn-large">
					</form>
				</div>
			</div>
			<div class="rooms col m9 right right-align">
				<div class="block-room">
					<p class="f50 hd-light white-text type-room">Standard</p>
					<div class="box-room clearfix">
						<img class="responsive-img" src="http://placehold.it/960x490">
						<div class="col m11 s11 center-block clearfix details">
							<div class="col m7 left">
								<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television wiredth satellite programmes.</p>
							</div>
							<div class="col m5">
								<div class="col m11 right center-block clearfix">
									<div class="clearfix">
										<p class="f18 hd-medium left italic"><i>EUR</i></p>
										<p class="f58 hd-medium left"><i>310.00</i></p>
										<p class="f18 hd-medium left period"><i>/ Night</i></p>
									</div>
									<a href="javascript:;" class="btn f18 a-light white-text center-block open-options"><span class="left">خيارات</span> <i class="material-icons">trending_flat</i></a>
								</div>
							</div>
						</div>
						<div class="show-options hide">
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="block-room">
					<p class="f50 hd-light type-room">Standard</p>
					<div class="box-room clearfix">
						<img class="responsive-img" src="http://placehold.it/960x490">
						<div class="col m11 s11 center-block clearfix details">
							<div class="col m7 left">
								<p class="f18 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television wiredth satellite programmes.</p>
							</div>
							<div class="col m5">
								<div class="col m11 right center-block clearfix">
									<div class="clearfix">
										<p class="f18 hd-medium left italic"><i>EUR</i></p>
										<p class="f58 hd-medium left"><i>310.00</i></p>
										<p class="f18 hd-medium left period"><i>/ Night</i></p>
									</div>
									<a href="javascript:;" class="btn f18 a-light white-text center-block open-options"><span class="left">Options</span> <i class="material-icons">trending_flat</i></a>
								</div>
							</div>
						</div>
						<div class="show-options hide">
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix details">
								<div class="col m11 s11 center-block clearfix">
									<div class="col m6 left">
										<div class="col m11">
											<p class="f28 hd-light">Flex with Breakfast</p>
											<p class="f16 a-light">Well-appointed with queen or twin beds our guestrooms fully carpeted. Extra copy required here.</p>
										</div>
									</div>
									<div class="col m6 right">
										<div class="col m7 left clearfix">
											<div class="clearfix">
												<p class="f13 hd-medium left italic"><i>EUR</i></p>
												<p class="f42 hd-medium left"><i>310.00</i></p>
												<p class="f13 hd-medium left period"><i>/ Night</i></p>
											</div>
										</div>
										<div class="col m5 right">
											<a href="javascript:;" class="btn f18 a-light white-text right"><span class="left">استمر</span> <i class="material-icons">trending_flat</i></a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-right col m3">
					<a href="./" class="hd-medium f44 white-text right-align"><span>الصفحة الرئيسية</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>معلومات عنا</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أماكن الإقامة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>المطاعم</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>مرافق</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خدمات</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>اتصل بنا</span></a>
				</div>
				<div class="text-left col m3">
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>خبرة</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>أحداث</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text right-align"><span>رجال الإعلام</span></a>
					<div class="newsletter right">
						<p class="white-text f18 a-light right-align">إذا كنت ترغب في البقاء حتى موعد مع الفندق، تلقي التحديثات والأخبار ومعرفة المزيد عن العروض الترويجية والعروض الحصرية وغيرها من المزايا، والاشتراك في النشرة الإخبارية عبر البريد الإلكتروني أدناه:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="أدخل عنوان بريدك الإلكتروني" class="white-text a-light right-align" required>
									<label for="email" data-error="خاطئ" class="right-align"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>Ar</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">En</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>