var app = {};

app.init = function init() {
	app.openMenu();
	app.mailSubscribe();
	app.scrollPage();
	app.datePicker();
	app.maps();
	app.showOptions();
	app.shareMedia();
	app.contact();

	if($('#menu').hasClass('hide')) {
		app.showMenu();
	}
};

$.fn.onEnterKey = function(closure) {
    $(this).keypress(function(event) {
        var code = event.keyCode ? event.keyCode : event.which;

        if (code == 13) {
            closure();
            return false;
        }
    });
};

app.openMenu = function() {
	var c = 0;
	$('.hamburger').off().on({
		'click' : function() {
			var $this = $(this),
				$modal = $('#modal'),
				docHeight = $(document).height(),
				posOffset = $(this).position(),
				posLogo = $('#hero .logo').position(),
				lang = document.getElementsByTagName("html")[0].getAttribute("lang"),
				top;

			if($this.closest('#hero').length < 1) {
				top = '45px';
			} else {
				top = posOffset.top;
			}

			$modal.find('.fundo').css('height', docHeight);

			if(c % 2 == 0) {
				$this.addClass('is-active');
				$modal.fadeIn(150).find('.hamburger').css({'left' : posOffset.left, 'top' : top});

				if($this.closest('#hero').length > 0) {
					$modal.find('.logo').css({'left' : posLogo.left, 'top' : posLogo.top, 'padding' : 0});
					$modal.find('.lang').css({'left' : posLogo.left});
				} else {
					$modal.find('.logo').css({'left' : $('#menu .logo').position().left, 'top' : $('#menu .logo').position().top, 'padding' : 0});
					$modal.find('.lang').css({'left' : $('#menu .logo').position().left});
				}

				if($(window).width() > 1199) {
					if(lang == 'ar') {
						$modal.find('.text-left').animate({'left' : '20%'}, 'fast', 'linear');
						$modal.find('.text-right').animate({'right' : '30%'}, 'fast', 'linear');
					} else {
						$modal.find('.text-left').animate({'left' : '30%'}, 'fast', 'linear');
						$modal.find('.text-right').animate({'right' : '20%'}, 'fast', 'linear');
					}
				}
			} else {
				$('.hamburger').removeClass('is-active');
				$modal.fadeOut(150).find('.hamburger').addClass('is-active');
				if($(window).width() > 1199) {
					$modal.find('.text-left').animate({'left' : '17%'}, 'fast', 'linear');
					$modal.find('.text-right').animate({'right' : '17%'}, 'fast', 'linear');
				}
			}

			c++;

			$(window).resize(function() {
				var posOffset = $this.position(),
					posLogo = $('#hero .logo').position();

				$modal.find('.hamburger').css('left', posOffset.left);

				if($this.closest('#hero').length < 1) {
					$modal.find('.logo').css({'left' : $('#menu .logo').position().left, 'top' : $('#menu .logo').position().top, 'padding' : 0});
				} else {
					$modal.find('.logo').css({'left' : posLogo.left, 'top' : posLogo.top, 'padding' : 0});
				}
			});
		}
	});
};

app.mailSubscribe = function() {
	$('#mailchimp #email').keypress(function(e) {
	//	e.preventDefault();
		var code = event.keyCode ? event.keyCode : event.which;

		if(code == 13) {
			alert($('#mailchimp #email').val());
			//return false;
		}
	});
};

app.scrollPage = function() {
	var winHeight = $(window).height(),
		body = $('html, body');

	$('.arrow-scroll').off().on({
		'click' : function() {
			body.stop().animate({scrollTop: winHeight}, '300', 'swing');
			$('.full-h').fadeIn(900);
		}
	});
};

app.datePicker = function() {
	var picker = [];
	for(var i = 0; i < $('.datepicker').length; i++) {
		picker[i] = new Pikaday({ field: $('.datepicker')[i], onSelect: function() { console.log(this.getMoment()) } });
	}
};

app.showMenu = function() {
	var winHeight = $('#hero').height();

	$(window).scroll(function() {
		if($(window).scrollTop() >= winHeight && !$('#modal').is(':visible')) {
			$('#menu').fadeIn();
		} else {
			$('#menu').fadeOut();
		}
	});
};

app.viewMore = function() {
	var winWid = $(window).width();

	$('.view-more').off().on({
		'click' : function() {
			window.$this = $(this);
			window.rowParent = $this.parents('.row-cards');
			window.row = rowParent.find('.card');
			window.pos = $this.parents('.card').index();
			var posRow = rowParent.index(),
				totalRow = $('.row-cards').length - 1,
				totalCards = $('.card').length,
				animate = row.eq(pos);

			$('.card').stop().animate({'width' : '49%'}, 'fast', 'linear').find('img').css('margin-top', 0);

			if(row.eq(pos).find('.extended').is(':visible')) {
				animate.stop().animate({'width' : '49%'}, 'fast', 'linear');
				if(totalCards % 2 == 0) {
					$('.card').last().css({'margin-top' : '.5rem', 'float' : 'right'});
				}
				if(posRow == totalRow) {
					if(pos == 0) {
						animate.css({'margin-bottom' : '-58px'});
					} else {
						animate.css({'margin-top' : '.5rem'});
					}
				}

				row.find('img').css({'margin-top' : '0'});

				row.eq(pos).find('.extended').fadeOut(function() {
					row.eq(pos).find('.name').fadeIn();
				});
			} else {
				animate.stop().animate({'width' : '100%'}, 'fast', 'linear');
				if(totalCards % 2 == 0 && pos == 1) {console.log('baaa');
					$('.card').last().css({'margin-top' : '.5rem', 'float' : 'right'});
				} else if(totalCards % 2 == 0 && pos == 0) {
					$('.card').last().css({'margin-top' : '108px', 'float' : 'left'});
				}
				if(posRow == totalRow) {
					if(pos == 0) {
						animate.css({'margin-bottom' : '60px'});
						row.eq(1).css('margin-top', '.5rem');
					} else {
						animate.css({'margin-top' : '108px'});
						row.eq(0).css('margin-bottom', '-58px');
					}
				}

				responsive();

				row.eq(pos).find('.name').fadeOut(function() {
					row.eq(pos).find('.extended').fadeIn();
				});
			}

			$('.name').show();
			$('.extended').hide();
		}
	});

	var responsive = function() {
		if(winWid >= 768 && winWid < 1000) {
			row.eq(pos).find('img').css({'margin-top' : '-19%'});
		} else if(winWid >= 1000 && winWid < 1366) {
			row.eq(pos).find('img').css({'margin-top' : '-12%'});
		} else if(winWid >= 1366 && winWid <= 1500) {
			row.eq(pos).find('img').css({'margin-top' : '-14.7%'});
		} else if(winWid > 1500 && winWid <= 1650) {
			row.eq(pos).find('img').css({'margin-top' : '-18%'});
		} else if(winWid > 1650) {
			row.eq(pos).find('img').css({'margin-top' : '-20%'});
		}
	};

	$(window).resize(function() {
		winWid = $(window).width();
		responsive();
	});
};

app.maps = function() {
	$('.maps').off().on({
		'click' : function() {
			$('.maps iframe').css('pointer-events', 'auto');
		},
		'mouseleave' : function() {
			$('.maps iframe').css('pointer-events', 'none');
		}
	});
};

app.showOptions = function() {
	$('.open-options').off().on({
		'click' : function() {
			var showOptions = $(this).parents('.block-room').find('.show-options');

			if(showOptions.is(':visible')) {
				showOptions.slideUp('150');
				$(this).removeClass('active').find('span').text('Options').siblings('.material-icons').show();
			} else {
				showOptions.slideDown('150');
				$(this).addClass('active').find('span').text('Show Less').siblings('.material-icons').hide();
			}
		}
	});
};

app.payment = function() {
	var $cardNumber = $('#card-number'),
		path = window.location.pathname.split('/');

	var cards = {
	    visa: /^4[0-9]{12}(?:[0-9]{3})/,
	    mastercard: /^5[1-5][0-9]{14}/,
	    amex: /^3[47][0-9]{13}/
	};

	function validCC(nr, cards) {
        for (window.card in cards) {
        	if (nr.match(cards[card])) {
        		$cardNumber.removeClass('invalid').addClass('valid');
        		if(card != $('.card-type.active').data('type')) {
					if(path[1] == 'en' || path[2] == 'en') {
			        	alert('CREDIT CARD INVALID!');
			        } else {
		        		alert('بطاقة الائتمان غير صالحة!');
			        }
        		}
        		return card;
        	} else {
				if(path[1] == 'en' || path[2] == 'en') {
		        	alert('CREDIT CARD INVALID!');
		        } else {
	        		alert('بطاقة الائتمان غير صالحة!');
		        }
		   		setTimeout(function() { $cardNumber.removeClass('valid').addClass('invalid'); }, 300);
		        return false;
        	}
        }
    }

	$cardNumber.off().on({
		'focusout' : function() {
			validCC($cardNumber.val(), cards);
		}
	});


	$('.card-type').off().on({
		'click' : function() {
			$(this).nextAll().each(function () {
				$(this).removeClass('active');
			});
			$(this).prevAll().each(function () {
				$(this).removeClass('active');
			});

			$(this).addClass('active');

			if($cardNumber.val() != '') {
				validCC($cardNumber.val(), cards);
			}
		}
	});

	$('.checkbox').off().on({
		'click' : function() {
			var checked = $('.checked');

			$('#agree').attr('checked', 'checked');
			if(checked.is(':visible')) { checked.hide(); } else { checked.show(); }
		}
	});

	$('button').off().on({
		'click' : function() {
			var $required = $('.big-block input:required'),
				errors = false;

			if(!$('#agree').is(':checked')) {
				if(path[1] == 'en' || path[2] == 'en') {
					alert('You must to read and agree with the Booking Conditions and Privacy Policy!');
				} else {
					alert('يجب عليك أن تقرأ وتوافق مع شروط الحجز و سياسة الخصوصية!');
				}
			} else {
				$required.map(function(){
			        if($required.val() && ($required.val() != 'Month' || $required.val() != 'Year')) {
			            $required.removeClass('invalid');
			        } else {
			            $required.addClass('invalid');
			            errors = true;
			        }
			    });

			    if(errors) {
					if(path[1] == 'en' || path[2] == 'en') {
				    	alert('You must to complete all fields!');
				    } else {
				    	alert('يجب عليك إكمال جميع الحقول!');
				    }
			    } else {
					if(path[1] == 'en' || path[2] == 'en') {
			    		alert('Success!');
				    } else {
			    		alert('نجاح!');
				    }
			    }
			}
		}
	})
};

app.shareMedia = function() {
	$('.share-media').off().on({
		'click' : function() {
		    var $this = $(this),
		    	url = $this.data('href'),
		    	winHeight = 400,
		    	winWidth = 555,
		    	winTop = (screen.height / 2) - (winHeight / 2),
		    	winLeft = (screen.width / 2) - (winWidth / 2);

  			return window.open('https://www.facebook.com/sharer.php?u='+url, 'Share on Facebook', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+winWidth+', height='+winHeight+', top='+winTop+', left='+winLeft);
		}
	});
};

app.scrollLanding = function() {
	$('#spy').hide();
	var pages = 6;
	var currentpage = 1;
	if (document.location.hash) { currentpage = parseInt(document.location.hash.replace('#', '')); }

	var nextpage = currentpage + 1; if (nextpage > pages) { nextpage = pages; }
	var prevpage = currentpage - 1; if (prevpage < 1) { prevpage = 1; }
	var animatingup = false;
	var animatingdown = false;

	$(document).ready(function() {
		resizeDiv();
	});

	window.onresize = function(event) {
		resizeDiv();
		scrolltocurrent();
		$('#page'+currentpage+' .full-h').fadeIn(900);
	}

	$(window).scroll(function(event) {
		if (animatingup==true) { return; }
		if (animatingdown==true) { return; }
		nextpage = currentpage + 1; if (nextpage > pages) { nextpage = pages; }
		prevpage = currentpage - 1; if (prevpage < 1) { prevpage = 1; }

		if(currentpage == 1) { $('#spy').hide(); } else { $('#spy').show(); }

		if (animatingup == false) {
			if ($(window).scrollTop()+$(window).height()>=$("#page"+(nextpage)).offset().top+50) {
				if(currentpage == 1) { $('#spy').hide(); } else { $('#spy').show(); }
				if (nextpage > currentpage) {
					var p2 = $("#page"+(nextpage));
					var pageheight = p2.position().top;
					animatingdown = true;
					$('html, body').stop().animate({ scrollTop: pageheight }, 500, function() { currentpage = nextpage; animatingdown = false; document.location.hash = currentpage;});
					return;
				}
			}
		}
		if (animatingdown == false) {
			if ($(window).scrollTop()<=$("#page"+(currentpage)).offset().top-50) {
				if (prevpage < currentpage) {
					var p2 = $( "#page"+(currentpage) );
					if(currentpage == 2) { $('#spy').hide(); } else { $('#spy').show(); }
					var pageheight = p2.position().top-$(window).height();
					animatingup = true;
					$('html, body').stop().animate({ scrollTop: pageheight }, 500, function() { currentpage = prevpage; animatingup = false; document.location.hash = currentpage;});
					return;
				}
			}
		}
		$('#page'+currentpage+' .full-h').fadeIn(900);
	});

	function scrolltocurrent() {
		var p2 = $( "#page"+(currentpage) );
		var pageheight = p2.position().top;
		$('html, body').stop().animate({ scrollTop: pageheight }, 200);
		if(currentpage == 1) { $('#spy').hide(); } else { $('#spy').show(); }
	}

	function resizeDiv() {
		vpw = $(window).width();
		vph = $(window).height();
		$('.page').css({'min-height': vph + 'px'});
	}
};

app.contact = function() {
	$('#send-contact').off().on({
		'click' : function(e) {
	        e.preventDefault();

			var $required = $('#contact input:required'),
				$textarea = $('#message'),
				lang = document.getElementsByTagName('html')[0].getAttribute('lang'),
				error = false;

			$required.map(function(){
		        if($required.val()) {
		            $required.removeClass('invalid');
		        } else {
		            $required.addClass('invalid');
		            error = true;
		        }
		    });

		    if($textarea.val()) {
		        $textarea.removeClass('invalid');
		    } else {
		        $textarea.addClass('invalid');
		        error = true;
		    }

		    if(!error) {
		    	$(this).attr('disabled', true);
		        $.post($(e.currentTarget).attr('action')+'.json', $(e.currentTarget).serialize(), function(response) {
		            if (response.status == 'error') {
		            	$(this).removeAttr('disabled');
		            	(lang == 'en') ? alert('Errors occurred while sending the message. Try again later.') : alert('حدث خطأ أثناء إرسال الرسالة. حاول مرة أخرى في وقت لاحق.');
		            } else {
		            	$(this).removeAttr('disabled');
		            	(lang == 'en') ? alert('Message sent successfully!') : alert('رسالة أرسلت بنجاح!');
		            }
		        }, 'json');

		        return false;
		    }
		}
	});
};

app.init();