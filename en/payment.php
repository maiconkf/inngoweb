<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two" class="no-booking">
		<?php include_once('includes/header.php'); ?>
		<div id="booking" class="clearfix black">
			<div class="container payment">
				<div class="col m7 center-block">
					<ul class="clearfix">
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">1. Select a Room</a></li>
						<li class="f24 left active"><a href="javascript:;" class="hd-light white-text">2. Payment</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">3. Review</a></li>
						<li class="f24 left"><a href="javascript:;" class="hd-light white-text">4. Confirmation</a></li>
					</ul>
				</div>
				<p class="hd-medium f124 white-text center-align"><i></i>Payment</p>
			</div>
		</div>
		<div class="container clearfix">
			<div class="col m7 w1200 right">
				<div class="col m10 right valign-wrapper valign-review">
					<p class="f180 hd-light right-align valign">Review</p>
				</div>
			</div>
			<div class="box-review col m5 big-block">
				<div class="col m11 clearfix center-block">
					<div class="col m10 center-block clearfix">
						<a href="javascript:;" class="logo">
							<img src="../dist/img/inn-go-hotel-bigger.png" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class=" center-block">
						</a>
						<div class="block">
							<p class="f20 hd-medium">9 nights</p>
							<p class="f20 a-light">From 12-04-2016 to 22-04-2016</p>
						</div>
						<div class="block">
							<p class="f20 hd-medium">Business Executive Room</p>
							<p class="f20 a-light">2 Adults</p>
						</div>
						<div class="block">
							<p class="f32 hd-medium">2,480.00 EUR</p>
							<p class="f20 a-light">/Total Stay</p>
						</div>
						<a href="javascript:;" class="btn center-block a-regular f18"><span class="left">Details</span> <i class="material-icons">trending_flat</i></a>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5">
					<div class="col m10 left valign-wrapper valign-guest">
						<p class="f180 hd-light left-align valign">Guest</p>
					</div>
				</div>
				<div class="box-guest col m7">
					<div class="col m12 right">
						<div class="col m10 center-block clearfix">
							<div class="input-field clearfix">
								<input placeholder="First Name" id="first-name" name="first-name" type="text" class="validate a-light f22" required>
								<label for="first-name" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Last Name" id="last-name" name="last-name" type="text" class="validate a-light f22" required>
								<label for="last-name" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Your Email" id="email" name="email" type="email" class="validate a-light f22" required>
								<label for="email" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Telephone" id="tel" name="tel" type="tel" class="validate a-light f22" required>
								<label for="tel" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Zipcode" id="zip" name="zip" type="text" class="validate a-light f22" required>
								<label for="zip" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Address" id="address" name="address" type="text" class="validate a-light f22" required>
								<label for="address" data-error="wrong"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5 right">
					<div class="col m10 right valign-wrapper valign-card">
						<p class="f180 hd-light left-align valign">Card</p>
					</div>
				</div>
				<div class="box-card col m7">
					<div class="col m12 left">
						<div class="col m11 center-block clearfix">
							<div class="col m11">
								<p class="a-light f19"><i>Credit Card Type</i></p>
							</div>
							<a href="javascript:;" class="card-type a-regular f22 left active" data-type="visa">VISA</a>
							<a href="javascript:;" class="card-type a-regular f22 left" data-type="mastercard">Master Card</a>
							<a href="javascript:;" class="card-type a-regular f22 left" data-type="amex">American Express</a>
							<div class="input-field clearfix">
								<input placeholder="Name of the Cardholder" id="name-card" name="name-card" type="text" class="validate a-light f22" required>
								<label for="first-name" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Credit Card Number" id="card-number" name="card-number" type="text" class="validate a-light f22" required>
								<label for="last-name" data-error="wrong"></label>
							</div>
							<div class="col m11 input-field">
								<p class="a-light f19"><i>Expire Date</i></p>
							</div>
							<div class="input-field col m4 date">
								<select>
									<option value="" disabled selected>Month</option>
									<option value="01">01</option>
									<option value="02">02</option>
									<option value="03">03</option>
									<option value="04">04</option>
									<option value="05">05</option>
									<option value="06">06</option>
									<option value="07">07</option>
									<option value="08">08</option>
									<option value="09">09</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
								</select>
		 					</div>
							<div class="input-field col m4 date">
								<select>
									<option value="" disabled selected>Year</option>
									<option value="2016">2016</option>
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
									<option value="2023">2023</option>
									<option value="2024">2024</option>
									<option value="2025">2025</option>
									<option value="2026">2026</option>
									<option value="2027">2027</option>
								</select>
		 					</div>
							<div class="input-field col m4 cvc">
								<input placeholder="CVV/CVC" id="cvc" name="cvc" type="number" class="validate a-light f22" required>
								<label for="last-name" data-error="wrong"></label>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix big-block">
				<div class="col m5">
					<div class="col m10 left valign-wrapper valign-final">
						<p class="f180 hd-light left-align valign">Final</p>
					</div>
				</div>
				<div class="box-final col m7">
					<div class="col m12 left">
						<div class="col m10 center-block clearfix">
							<div class="input-field clearfix">
								<input placeholder="Any additional information? (Optional)" id="name-card" name="name-card" type="text" class="validate a-light f22">
								<label for="first-name" data-error="wrong"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Flight Number (Optional)" id="card-number" name="card-number" type="text" class="validate a-light f22">
								<label for="last-name" data-error="wrong"></label>
							</div>
							<div class="col m12 center-block clearfix box-agree">
								<p class="f18 a-light col m10 left">I have understood and agree to the <a href="javascript:;" target="_blank" class="a-regular"><b>Booking Conditions</b></a> and <a href="javascript:;" target="_blank" class="a-regular"><b>Privacy Policy</b></a></p>
								<div class="right col m2">
								    <input type="checkbox" id="agree" required />
								    <div class="checkbox">
								    	<span class="checked icon"></span>
								    </div>
								</div>
							</div>
							<button type="submit" class="btn center-block"><span class="left">Continue to Review Details</span> <i class="material-icons">trending_flat</i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m3">
					<a href="./" class="hd-medium f44 white-text"><span>Home</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>About Us</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Accommodations</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Restaurants</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Facilities</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Services</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Contact Us</span></a>
				</div>
				<div class="text-right col m3">
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Experiences</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Events</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Media</span></a>
					<div class="newsletter">
						<p class="white-text f18 a-light">If you want to stay up to date with the Hotel, receive updates, news and find our about promotions, exclusive offers and other benefits, sign up for our email newsletter below:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="Enter your email address" class="white-text a-light" required>
									<label for="email" data-error="wrong"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>En</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">Ar</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>
			app.payment();
			app.init();
			$('select').material_select();
		</script>
	</body>
</html>