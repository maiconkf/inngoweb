<div id="menu">
	<div class="container">
		<div class="col m3">
			<a href="javascript:;" class="logo left">
				<img src="../dist/img/inn-go-hotel-black.png" srcset="../dist/img/inn-go-hotel-black.png 1x, ../dist/img/inn-go-hotel-black@2x.png 2x, ../dist/img/inn-go-hotel-black@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="144" height="40">
			</a>
		</div>
		<div class="col m1 right">
			<button class="hamburger hamburger--squeeze right open-modal" type="button" aria-label="Menu" aria-controls="navigation">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
			</button>
		</div>
		<div class="check-booking col m8 left">
			<div class="left calendar">
				<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
				<div class="box-calendar"><span class="icon-calendar icon"></span></div>
			</div>
			<div class="left calendar">
				<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
				<div class="box-calendar"><span class="icon-calendar icon"></span></div>
			</div>
			<a href="javascript:;" class="btn f16 a-regular white-text left">Check availability</a>
		</div>
	</div>
</div>