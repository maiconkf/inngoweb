<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="spy" class="hide-on-small-only">
			<a href="#page2"></a>
			<a href="#page3"></a>
			<a href="#page4"></a>
			<a href="#page5"></a>
			<a href="#page6"></a>
		</div>
		<div id="page1" class="landing page">
			<div class="content full-h">
				<div class="row">
					<div class="main-text">
						<div class="center-block center-align">
							<img src="../dist/img/inn-go-hotel-golden--big.png" srcset="../dist/img/inn-go-hotel-golden--big.png 1x, ../dist/img/inn-go-hotel-golden--big@2x.png 2x, ../dist/img/inn-go-hotel-golden--big@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="315" height="315" class="responsive-img">
						</div>
					</div>
					<div class="center-block box-scroll">
						<p class="f20 dark-golden center-align a-lightitalic">Scroll to discover</p>
						<a href="javascript:;" class="arrow-scroll bounce icon"></a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern-landing" class="clearfix landing-page">
			<div id="page2" class="scrollspy page">
				<div class="full-h container">
					<div class="header-landing clearfix">
						<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" srcset="../dist/img/inn-go-hotel-golden.png 1x, ../dist/img/inn-go-hotel-golden@2x.png 2x, ../dist/img/inn-go-hotel-golden@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="84" height="84"></a>
					</div>
					<h1 class="f141 av-roman center-align article dark-golden">We build  amazing hotels</h1>
				</div>
			</div>
			<div id="page3" class="discover scrollspy page">
				<div class="full-h container">
					<div class="header-landing clearfix">
						<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" srcset="../dist/img/inn-go-hotel-golden.png 1x, ../dist/img/inn-go-hotel-golden@2x.png 2x, ../dist/img/inn-go-hotel-golden@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="84" height="84"></a>
					</div>
					<h2 class="f18 av-roman center-align article article--subtitle article--title dark-golden">Kuwait City</h2>
					<h3 class="f87 av-roman center-align article article--title dark-golden">Kuwait Plaza Hotel</h3>
					<div class="clearfix text-landing">
						<div class="col m6 layer" data-depth="0.2">
							<img class="responsive-img" src="../dist/img/facade-inn-and-go-hotel.jpg"  alt="Facade - Inn & Go - Kuwait Hotel Paza" title="Facade - Inn & Go - Kuwait Hotel Paza">
						</div>
						<div class="col m6">
							<div class="center-block">
								<p class="f21 av-book dark-golden">A brilliant experience for business executives and casual travelerers visiting Kuwait Cities. World-class facilities, a staff prepared to serve customer's needs with excellence and fantastic reviews, inn & Go is a go-to destination.</p>
								<a href="javascript:;" class="f20 a-light btn-large btn-large--landing "><span class="left">Discover </span> <i class="material-icons">trending_flat</i></a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="page4" class="scrollspy page">
				<div class="full-h container">
					<div class="header-landing clearfix">
						<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" srcset="../dist/img/inn-go-hotel-golden.png 1x, ../dist/img/inn-go-hotel-golden@2x.png 2x, ../dist/img/inn-go-hotel-golden@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="84" height="84"></a>
					</div>
					<h2 class="f18 av-light center-align article article--subtitle dark-golden">About Us</h2>
					<h3 class="f53 av-roman center-align article article--title article--subtitle dark-golden">We are a Kuwaiti company committed to creating world-class hotels. We believe leisure is an art and our customers live remarkable experiences</h3>
					<div class="clearfix">
						<div class="col m6 right">
							<div class="col m12 center-block">
								<p class="f21 av-book dark-golden">Born in 1995 and now present in more than 15 destinations, we have built a story around great services that impress people.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="page5" class="scrollspy page">
				<div class="full-h container">
					<div class="header-landing clearfix">
						<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" srcset="../dist/img/inn-go-hotel-golden.png 1x, ../dist/img/inn-go-hotel-golden@2x.png 2x, ../dist/img/inn-go-hotel-golden@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="84" height="84"></a>
					</div>
					<h2 class="f18 av-roman center-align article article--subtitle dark-golden">Media Center</h2>
					<h3 class="f53 av-roman center-align article article--title dark-golden left">Updates and news about Inn & Go. For press inquiries, pr@innandgo.com</h3>
					<div class="clearfix medias">
						<div class="col m7">
							<div class="card">
								<div class="card-image" style="background: url(//placehold.it/732x336);">
									<span class="f44 av-roman card-title">The story behind designing our website</span>
								</div>
								<div class="card-content">
									<div class="card-date">
										<em class="f15 a-lightitalic">March 05, 2016</em>
									</div>
									<div class="f15 av-light card-text">
										<p>Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes. Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes...</p>
									</div>
								</div>
								<div class="card-action clearfix">
									<a href="javascript:;" class="f20 a-regular btn-large btn-large--landing left"><span class="left">Continue Reading </span> <i class="material-icons">trending_flat</i></a>
									<a href="javascript:;" data-href="http://globo.com" class="right f17 a-lightitalic share-media">Share <span class="icon icon-share right"></span></a>
								</div>
							</div>
							<div class="card">
								<div class="card-image" style="background: url(//placehold.it/732x336);">
									<span class="f44 av-roman card-title">The story behind designing our website</span>
								</div>
								<div class="card-content">
									<div class="card-date">
										<em class="f15 a-lightitalic">March 05, 2016</em>
									</div>
									<div class="f15 av-light card-text">
										<p>Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes. Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes...</p>
									</div>
								</div>
								<div class="card-action clearfix">
									<a href="javascript:;" class="f20 a-regular btn-large btn-large--landing left"><span class="left">Continue Reading </span> <i class="material-icons">trending_flat</i></a>
									<a href="javascript:;" data-href="http://globo.com" class="right f17 a-lightitalic share-media">Share <span class="icon icon-share right"></span></a>
								</div>
							</div>
						</div>
						<div class="col m5">
							<div class="quote">
								<blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">Sunsets don&#39;t get much better than this one over <a href="https://twitter.com/GrandTetonNPS">@GrandTetonNPS</a>. <a href="https://twitter.com/hashtag/nature?src=hash">#nature</a> <a href="https://twitter.com/hashtag/sunset?src=hash">#sunset</a> <a href="http://t.co/YuKy2rcjyU">pic.twitter.com/YuKy2rcjyU</a></p>&mdash; US Dept of Interior (@Interior) <a href="https://twitter.com/Interior/status/463440424141459456">5 de maio de 2014</a></blockquote><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
							</div>

							<div class="quote">
								<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="6" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:658px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);"><div style="padding:8px;"> <div style=" background:#F8F8F8; line-height:0; margin-top:40px; padding:50% 0; text-align:center; width:100%;"> <div style=" background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACwAAAAsCAMAAAApWqozAAAAGFBMVEUiIiI9PT0eHh4gIB4hIBkcHBwcHBwcHBydr+JQAAAACHRSTlMABA4YHyQsM5jtaMwAAADfSURBVDjL7ZVBEgMhCAQBAf//42xcNbpAqakcM0ftUmFAAIBE81IqBJdS3lS6zs3bIpB9WED3YYXFPmHRfT8sgyrCP1x8uEUxLMzNWElFOYCV6mHWWwMzdPEKHlhLw7NWJqkHc4uIZphavDzA2JPzUDsBZziNae2S6owH8xPmX8G7zzgKEOPUoYHvGz1TBCxMkd3kwNVbU0gKHkx+iZILf77IofhrY1nYFnB/lQPb79drWOyJVa/DAvg9B/rLB4cC+Nqgdz/TvBbBnr6GBReqn/nRmDgaQEej7WhonozjF+Y2I/fZou/qAAAAAElFTkSuQmCC); display:block; height:44px; margin:0 auto -44px; position:relative; top:-22px; width:44px;"></div></div> <p style=" margin:8px 0 0 0; padding:0 4px;"> <a href="https://www.instagram.com/p/tsxp1hhQTG/" style=" color:#000; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none; word-wrap:break-word;" target="_blank">We’re putting the Weekend Hashtag Project on hold this weekend. Instead, we’re challenging people around the world to participate in the 10th Worldwide InstaMeet! Grab a few good friends or meet up with a larger group in your area and share your best photos and videos from the InstaMeet with the #WWIM10 hashtag for a chance to be featured on our blog Monday morning. Be sure to include the name of the location where your event took place along with the unique hashtag you&#39;ve chosen for your InstaMeet in your caption. Photo by @sun_shinealight</a></p> <p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">Uma foto publicada por Instagram (@instagram) em <time style=" font-family:Arial,sans-serif; font-size:14px; line-height:17px;" datetime="2014-10-03T18:00:13+00:00">Out 3, 2014 às 11:00 PDT</time></p></div></blockquote><script async defer src="//platform.instagram.com/en_US/embeds.js"></script>
							</div>
							<div class="quote">
								<div id="fb-root"></div><script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.3";  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script><div class="fb-post" data-href="https://www.facebook.com/20531316728/posts/10154009990506729/" data-width="509"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/20531316728/posts/10154009990506729/">Publicado por <a href="https://www.facebook.com/facebook/">Facebook</a> em&nbsp;<a href="https://www.facebook.com/20531316728/posts/10154009990506729/">Quinta, 27 de agosto de 2015</a></blockquote></div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="page6" class="scrollspy page">
				<div class="full-h container">
					<div class="header-landing clearfix">
						<a href="javascript:;" class="left"><img src="../dist/img/inn-go-hotel-golden.png" srcset="../dist/img/inn-go-hotel-golden.png 1x, ../dist/img/inn-go-hotel-golden@2x.png 2x, ../dist/img/inn-go-hotel-golden@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="84" height="84"></a>
					</div>
					<h2 class="f53 av-roman center-align article article--subtitle dark-golden">Want to contact us?</h2>
					<div class="clearfix">
						<form id="contact" class="col m5 center-block clearfix" method="post">
							<div class="input-field clearfix">
								<input placeholder="Name" id="name" name="name" type="text" class="validate a-light f22 golden" required="">
								<label for="name" data-error="wrong" class="active"></label>
							</div>
							<div class="input-field clearfix">
								<input placeholder="Email" id="email" name="email" type="email" class="validate a-light f22" required="">
								<label for="email" data-error="wrong" class="active"></label>
							</div>
							<div class="input-field clearfix">
								<textarea placeholder="What's your message?" id="message" name="message" class="validate a-light f22 materialize-textarea" required=""></textarea>
								<label for="message" data-error="wrong" class="active"></label>
							</div>
							<div class="center-align">
								<input type="submit" value="Get in touch" class="f20 a-regular btn-large btn-large--landing">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>
			$('.discover').parallax();
			$('.scrollspy').scrollSpy();
			app.scrollLanding();
		</script>
	</body>
</html>