<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="menu" class="hide sticky">
			<div class="container">
				<div class="col m3">
					<a href="javascript:;" class="logo left">
						<img src="../dist/img/inn-go-hotel-black.png" srcset="../dist/img/inn-go-hotel-black.png 1x, ../dist/img/inn-go-hotel-black@2x.png 2x, ../dist/img/inn-go-hotel-black@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="144" height="40">
					</a>
				</div>
				<div class="col m1 right">
					<button class="hamburger hamburger--squeeze right open-modal" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="check-booking col m8 left">
					<div class="left calendar">
						<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<div class="left calendar">
						<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<a href="javascript:;" class="btn f16 a-regular white-text left">Check availability</a>
				</div>
			</div>
		</div>
		<div id="hero" class="intern">
			<div class="content">
				<div class="clearfix">
					<a href="home">
						<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class="logo ">
					</a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>En</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">Meetings and Conferences</h2>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">Check availability</a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern" class="clearfix events">
			<div class="container clearfix">
				<div class="col m7 info">
					<h1 class="f200 hd-medium">Events</h1>
					<div class="col m11 text-events">
						<p class="f22 a-light">
							Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.
						</p>
					</div>
				</div>
			</div>
			<img class="responsive-img full-image" src="http://placehold.it/1920x760">
			<div class="container clearfix">
				<div class="col m7 center-block">
					<h2 class="f60 hd-medium center-align">Why choose us?</h2>
					<div class="text">
						<p class="f18 a-light">
							Each Member purchases one week in high season which remains the same each year for the duration of the lease. Each Member is also entitled to four planned vacation weeks throughout the year which are chosen several months in advance. To avoid conflicts, an equitable rotation system is used. The lease is for a period of 75 years and Members can sell or transfer their membership anytime.
						</p>
						<p class="f18 a-light">
							For any weeks that are not occupied by the Member, friends or family, these can be released back to the hotel for rental. Each member receives 60% of the rental and this money is credited to the Club Member’s account. This money can be used to pay for extra's incurred at the hotel (car rental, flights, bar and restaurant bills, etc.), pay the quarterly due fees or can be credited back to the Member.
						</p>
						<p class="f18 a-light">
							The Club also provides storage for all Members. Personal belongings can be left in special containers in the hotel’s secure air-conditioned storage facility. The boxes will be delivered to the villa prior to arrival and taken away at the end of the stay. Beach Club: umbrellas, beach towels, snorkelling gear, water activities, beverage services and reserved beach chairs.
						</p>
					</div>
				</div>
				<div class="our-events clearfix">
					<div class="col m6 left">
						<div class="col m11 center-block">
							<h2 class="f58 hd-medium">Our events room</h2>
							<p class="f18 a-light">
								Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes, 24 hours in- room dining, laundry service, safety deposit box.
							</p>
							<a href="javascript:;" class="btn white-text f20 a-light">Learn More</a>
						</div>
					</div>
					<div class="col m6 right">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Business Suite</h2>
							<p class="f18 a-light">
								High speed internet access, Satellite TV including international channels, in-room safe, individually controlled air conditioning system, IDD telephone line, coffee & tea facilities, mini bar, daily news papers and hair dryer. In-room personal computer included in certain rooms.
							</p>
						</div>
					</div>
					<div class="col m6 left">
						<img class="responsive-img" src="http://placehold.it/659x539">
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Deluxe</h2>
							<p class="f18 a-light">
								Deluxe rooms come with a range of upgraded value-added benefits such as Personal Computer in the room to suit business or leisure travelers. In addition, rooms on the 10th floor are equipped with extra toiletries, bathtub, and separate shower.
							</p>
						</div>
					</div>
					<div class="col m6 right last">
						<div class="col m12 center-block">
							<img class="responsive-img" src="http://placehold.it/599x737">
						</div>
						<div class="col m11 center-block">
							<h2 class="f60 hd-medium">Executive</h2>
							<p class="f18 a-light">
								Each of our Executive rooms is thoughtfully designed for both the business and leisure traveler. Large bathrooms with shower and Jacuzzi are also available.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="quotes clearfix">
				<div class="col m6 right">
					<div class="col m10 valign-wrapper right">
						<p class="f180 hd-light valign">Quote</p>
					</div>
				</div>
				<div class="col m6 box left">
					<div class="col m8 center-block">
						<form id="quotes" method="post">
							<div class="input-field col m12">
								<select>
									<option value="" disabled selected>Select Event Type</option>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
								</select>
		 					</div>
							<div class="input-field col m12">
								<input type="tel" id="atendees" name="atendees" class="f22 a-light" placeholder="Expected Attendees" required>
							</div>
							<div class="input-field col m12">
								<p class="col m12 f19 a-light"><i>Event Dates</i></p>
								<div class="box-date col m5 left">
									<input type="text" id="start" name="start" class="f22 a-light datepicker col m12" placeholder="Start" required>
									<span class="caret"></span>
								</div>
								<div class="box-date col m5 left">
									<input type="text" id="end" name="end" class="f22 a-light datepicker col m12" placeholder="End" required>
									<span class="caret"></span>
								</div>
							</div>
							<div class="input-field col m12">
								<input type="text" id="name" name="name" class="f22 a-light" placeholder="Your Name" required>
							</div>
							<div class="input-field col m12">
								<input type="text" id="name" name="name" class="f22 a-light" placeholder="Company/Organization" required>
							</div>
							<div class="input-field col m12">
								<input type="email" id="email" name="email" class="f22 a-light" placeholder="Email" required>
								<label for="email" data-error="wrong"></label>
							</div>
							<div class="input-field col m12">
								<input type="tel" id="tel" name="tel" class="f22 a-light" placeholder="Phone Number" required>
							</div>
							<div class="input-field col m12">
								<input type="text" id="additional" name="additional" class="f22 a-light" placeholder="Additional Information (Optional)"/>
							</div>
							<br clear="both">
							<input type="submit" value="Get a Quote Now" class="btn f20 white-text center-block" />
						</form>
	 				</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m3">
					<a href="./" class="hd-medium f44 white-text"><span>Home</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>About Us</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Accommodations</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Restaurants</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Facilities</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Services</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Contact Us</span></a>
				</div>
				<div class="text-right col m3">
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Experiences</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text active">Events</a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Media</span></a>
					<div class="newsletter">
						<p class="white-text f18 a-light">If you want to stay up to date with the Hotel, receive updates, news and find our about promotions, exclusive offers and other benefits, sign up for our email newsletter below:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="Enter your email address" class="white-text a-light" required>
									<label for="email" data-error="wrong"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>En</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">Ar</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
		<script>$('select').material_select();</script>
	</body>
</html>