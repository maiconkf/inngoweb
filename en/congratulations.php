<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two" class="no-booking">
		<?php include_once('includes/header.php'); ?>
		<div id="intern" class="clearfix">
			<div class="congratulations container clearfix">
				<h1 class="f145 hd-medium center-align">Congratulations</h1>
				<div class="col m6 center-block clearfix">
					<div class="col m11">
						<p class="f18 a-light center-align">
							Well-appointed with queen or twin beds our guestrooms fully carpeted, high speed wired internet, international or local direct dial, mini bar, color television with satellite programmes, 24 hours in- room dining, laundry service, safety deposit box.
						</p>
						<a href="../tpls/" class="btn"><span class="left">Go to the Home</span> <i class="material-icons">trending_flat</i></a>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m3">
					<a href="./" class="hd-medium f44 white-text"><span>Home</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>About Us</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Accommodations</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Restaurants</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Facilities</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Services</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Contact Us</span></a>
				</div>
				<div class="text-right col m3">
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Experiences</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Events</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Media</span></a>
					<div class="newsletter">
						<p class="white-text f18 a-light">If you want to stay up to date with the Hotel, receive updates, news and find our about promotions, exclusive offers and other benefits, sign up for our email newsletter below:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="Enter your email address" class="white-text a-light" required>
									<label for="email" data-error="wrong"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>En</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">Ar</a>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>