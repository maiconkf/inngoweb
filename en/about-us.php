<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="UTF-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	    <title>Inn & Go</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="../dist/css/styles.min.css">
	</head>
	<body id="layout-two">
		<div id="menu" class="hide sticky">
			<div class="container">
				<div class="col m3">
					<a href="javascript:;" class="logo left">
						<img src="../dist/img/inn-go-hotel-black.png" srcset="../dist/img/inn-go-hotel-black.png 1x, ../dist/img/inn-go-hotel-black@2x.png 2x, ../dist/img/inn-go-hotel-black@3x.png 3x" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" width="144" height="40">
					</a>
				</div>
				<div class="col m1 right">
					<button class="hamburger hamburger--squeeze right open-modal" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="check-booking col m8 left">
					<div class="left calendar">
						<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<div class="left calendar">
						<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
						<div class="box-calendar"><span class="icon-calendar icon"></span></div>
					</div>
					<a href="javascript:;" class="btn f16 a-regular white-text left">Check availability</a>
				</div>
			</div>
		</div>
		<div id="hero" class="intern about">
			<div class="content">
				<div class="clearfix">
					<a href="home">
						<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza" class="logo ">
					</a>
					<div class="right block-menu">
						<p class="lang left a-light white-text f15"><u>En</u></p>
						<button class="hamburger hamburger--squeeze open-modal right" type="button" aria-label="Menu" aria-controls="navigation">
							<span class="hamburger-box">
								<span class="hamburger-inner"></span>
							</span>
						</button>
					</div>
				</div>
				<div class="row main-text clearfix">
					<div class="col m7 center-block">
						<h2 class="f60 white-text center-align hd-medium">About Us</h2>
					</div>
				</div>
				<div class="row">
					<div class="check-booking">
						<div class="clearfix left">
							<div class="left calendar">
								<input type="text" name="checkin" class="checkin datepicker" value="Check-In">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
							<div class="left calendar">
								<input type="text" name="checkout" class="checkout datepicker" value="Check-Out">
								<div class="box-calendar"><span class="icon-calendar icon"></span></div>
							</div>
						</div>
						<a href="javascript:;" class="btn f16 a-regular white-text left">Check availability</a>
					</div>
				</div>
			</div>
		</div>
		<div id="intern" class="clearfix">
			<div class="our-story container clearfix">
				<h1 class="f200 hd-medium">Our Story</h1>
				<div class="col m7">
					<div class="col m11">
						<p class="f22 a-light">
							Inn and Go Plaza Kuwait is a mere 20 minutes from the airport and in the vicinity of The Grand Mosque of Kuwait, Kuwait Tower, Liberation Tower, and the Scientific Museum. Here for work? Enjoy high-speed internet access, meeting facilities and a ballroom for functions that can hold up to 250 guests.
						</p>
					</div>
				</div>
			</div>
			<div class="content-about">
				<img class="responsive-img" src="http://placehold.it/1920x760">
				<div class="container">
					<h2 class="f60 hd-medium center-align">It started in 1986</h2>
					<div class="col m8 center-block">
						<div class="col m12">
							<p>
								Each Member purchases one week in high season which remains the same each year for the duration of the lease. Each Member is also entitled to four planned vacation weeks throughout the year which are chosen several months in advance. To avoid conflicts, an equitable rotation system is used. The lease is for a period of 75 years and Members can sell or transfer their membership anytime.
							</p>
							<p>
								For any weeks that are not occupied by the Member, friends or family, these can be released back to the hotel for rental. Each member receives 60% of the rental and this money is credited to the Club Member’s account. This money can be used to pay for extra's incurred at the hotel (car rental, flights, bar and restaurant bills, etc.), pay the quarterly due fees or can be credited back to the Member.
							</p>
							<p>
								The Club provides a full time concierge service, to arrange flights to and from Saint-Barthélemy, VIP Service, shopping delivered to the villa prior to arrival, babysitting, restaurant reservations, car rental, in-villa personal coach, etc.
							</p>
							<p>
								The Club also provides storage for all Members. Personal belongings can be left in special containers in the hotel’s secure air-conditioned storage facility. The boxes will be delivered to the villa prior to arrival and taken away at the end of the stay. Beach Club: umbrellas, beach towels, snorkelling gear, water activities, beverage services and reserved beach chairs.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include_once('includes/footer.php'); ?>
		<div id="modal" class="hide">
			<div class="fundo close"></div>
			<div class="container">
				<div class="col m2 left logo">
					<img src="../dist/img/inn-go-logo.png" srcset="../dist/img/inn-go-logo.png 1x, ../dist/img/inn-go-logo@2x.png 2x, ../dist/img/inn-go-logo@3x.png 3x" width="164" height="45" alt="Inn & Go - Kuwait Hotel Paza" title="Inn & Go - Kuwait Hotel Paza">
				</div>
				<div class="col m2 right close-icon">
					<button class="hamburger hamburger--squeeze is-active right close" type="button" aria-label="Menu" aria-controls="navigation">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="text-left col m3">
					<a href="./" class="hd-medium f44 white-text"><span>Home</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text active">About Us</a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Accommodations</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Restaurants</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Facilities</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Services</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Contact Us</span></a>
				</div>
				<div class="text-right col m3">
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Experiences</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Events</span></a>
					<a href="javascript:;" class="hd-medium f44 white-text"><span>Media</span></a>
					<div class="newsletter">
						<p class="white-text f18 a-light">If you want to stay up to date with the Hotel, receive updates, news and find our about promotions, exclusive offers and other benefits, sign up for our email newsletter below:</p>
						<div class="row clearfix">
							<form id="mailchimp" method="post">
								<div class="input-field col s12">
									<input id="email" type="email" placeholder="Enter your email address" class="white-text a-light" required>
									<label for="email" data-error="wrong"></label>
									<input type="submit" id="sub" class="hide">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="lang">
					<a href="javascript:;" class="f18 a-light white-text left"><u>En</u></a>
					<a href="javascript:;" class="f18 a-light white-text left">Ar</a>
				</div>
			</div>
		</div>
		<script src="../dist/js/scripts.min.js"></script>
	</body>
</html>